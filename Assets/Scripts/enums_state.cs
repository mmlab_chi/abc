﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public enum SlingshotState
    {
        Idle,
        UserPulling,
        BirdFlying
    }

    public enum GameState
    {
        Start, 
        BirdMovingToSlingshot, 
        Playing, 
        Won, 
        Lost
    }

    public enum BirdState
    {
        On, 
        Off,
        Wait,
        Drag,
        beforeThrown,       
        Thrown,
        Die
    }
}
