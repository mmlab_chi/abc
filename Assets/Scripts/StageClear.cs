﻿using UnityEngine;
using System.Collections;

public class StageClear : MonoBehaviour {
    GameManage gm;
    GameObject go;
    void Start()
    {
        go = GameObject.FindGameObjectWithTag("MainCamera");
        gm = go.GetComponent<GameManage>();
    }
    public void UpdateStageClear(int newstage)
    {
        int a = (newstage-1);
        if (gm.score > PlayerPrefs.GetInt("HighScore" + a.ToString()))
        {
            PlayerPrefs.SetInt("HighScore"+a.ToString(), gm.score);
        }
        PlayerPrefs.SetInt("CurStage", newstage);
        PlayerPrefs.Save();
    }

    public void UpdateLevelClear()
    {
        int newlevel;
        newlevel = PlayerPrefs.GetInt("CurLevel") + 1;
        PlayerPrefs.SetInt("CurLevel", newlevel);
        PlayerPrefs.SetInt("CurStage", 1);
        PlayerPrefs.Save();
    }
}
