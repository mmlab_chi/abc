﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class  MovePanel : MonoBehaviour {
    public GameObject panel;
        
    public void movepanel(int Move)//move into screen: 0
    {
        if(Move == 0)
        {
            panel.transform.positionTo(0.5f, new Vector3(panel.transform.position.x + 2f, panel.transform.position.y, panel.transform.position.z));
        }
        else //move == 1: move out of the screen
        {
            panel.transform.positionTo(0.5f, new Vector3(panel.transform.position.x - 2f, panel.transform.position.y, panel.transform.position.z));
        }
    }
}
