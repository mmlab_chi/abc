﻿using UnityEngine;
using System.Collections;

public class BonusScoreEffect : MonoBehaviour {
    int timetolive = 0;
   
	void Update ()
    {
        if (timetolive >= 100f)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.scaleTo(3f, 3f);
            transform.Translate(Vector3.up * 0.5f * Time.deltaTime);
            timetolive++;
        }        
	}
}
