﻿using UnityEngine;
using System.Collections;

public class Feather : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(10, 100), Random.Range(10, 100)));
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -3)
        {
            enabled = false;
            Destroy(this.gameObject);
        }
	}
}
