﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManage : MonoBehaviour {
    public Transform leftCam; 
    public Transform rightCam;
   
    public Vector3 cameraposition;
    public dragBird nowbird;
    public dragBird lastbird = null;
    private GameState stateOfGame;
    private int i = 0;
    public Canvas m_canvas;
    public Text m_text;
    public GameObject WinGamePanel;
    public GameObject LoseGamePanel;
    public Text textScore;
    public Text HighScore;
    public GameObject Star1;
    public GameObject Star2;
    public GameObject Star3;


    private Text scoreAlive;
        [HideInInspector]
    public List<GameObject> Birds;
    public List<GameObject> Woods;
    public List<GameObject> Pigs;
    public List<Pig> LPigs;
    public int gameresult;
    public int score;
    public int _countpig;
    public string s;
    private int _countbird;
    public string levelName;
	// Use this for initialization
	void Start () {
        Camera.main.transform.position = rightCam.transform.position;
        transform.positionTo(2f,leftCam.transform.position);
       
        Birds =  new List<GameObject> (GameObject.FindGameObjectsWithTag("bird"));
        Woods = new List<GameObject>(GameObject.FindGameObjectsWithTag("Wood"));
        Pigs = new List<GameObject>(GameObject.FindGameObjectsWithTag("Pig"));
        
        stateOfGame = GameState.Start;
        nowbird = Birds[i].GetComponent<dragBird>();
        score = 0;
        _countpig = 0;
       
        m_canvas.enabled = true;
        m_text.enabled = true;
        WinGamePanel.SetActive(false);
        LoseGamePanel.SetActive(false);

        s = "Score: " + score.ToString();
        m_text.text = s;
        _countbird = 0;
        gameresult = 2;
        
        levelName = Application.loadedLevelName;
        int highscore = PlayerPrefs.GetInt("HighScore"+levelName);
        Debug.Log("HighScore" + levelName + " " + highscore);
        if (PlayerPrefs.HasKey("HighScore" + levelName) == true)
        {
            Debug.Log("haskeyyyyyyyyyyyyyyyyyyy");
        }
        HighScore.text = "HIGHSCORE: " + highscore.ToString();
	}

    void Update()
    {
        if (nowbird.BirdStateAtGame == BirdState.Die)
        {
            float timemovetostartposition;
            timemovetostartposition = Vector2.Distance(cameraposition, leftCam.transform.position) / 1f;
            transform.positionTo(timemovetostartposition, leftCam.transform.position);
            i++;

            if (i == Birds.Count && _countpig < Pigs.Count)
            {
                gameresult = 0;

                StartCoroutine(LoseGame());
            }
            Debug.Log("game result = " + gameresult);
           
            if (gameresult == 1)//win game
            {
                StartCoroutine(FindBirdAlive());
               
            }
        }
        
        if (_countpig == Pigs.Count)
        {
            gameresult = 1;//win           
        }
           
    }

    IEnumerator LoseGame()
    {
        yield return new WaitForSeconds(3f);
        LoseGamePanel.SetActive(true);
    }

    IEnumerator FindBirdAlive()
    {
        List<GameObject> AliveBird = new List<GameObject>(GameObject.FindGameObjectsWithTag("bird"));
        dragBird Bir = null;
        foreach (GameObject b in AliveBird)
        {
            Bir = b.GetComponent<dragBird>();
            Bir.Scorealive.SetActive(true);
            score += 10000;
            updateScore();
        }
        if (PlayerPrefs.GetInt("HighScore" + levelName) < score)
        {
            Debug.Log("cap nhat high score");
            PlayerPrefs.SetInt("HighScore" + levelName, score);
            PlayerPrefs.Save();
            int h = PlayerPrefs.GetInt("HighScore" + levelName); ;
            Debug.Log("This level high score is " + h);
        }
        yield return new WaitForSeconds(2f);
        WinGamePanel.SetActive(true);

        textScore.lineSpacing = 0.5f;
        StartCoroutine(StarEnable(1f));
        s = "SCORE: " + score.ToString();
        textScore.text = s;
        textScore.fontSize = 25;
    }

    void LateUpdate()
    {
        nowbird = Birds[i].GetComponent<dragBird>();
       
        nowbird.numberOfbird = i + 1;
        if (nowbird.StateOfGameinBird == GameState.Start && nowbird.isAnim == false)
        {
            nowbird.StateOfGameinBird = GameState.BirdMovingToSlingshot;
            nowbird.BirdStateAtGame = BirdState.On;
        }
        if (nowbird.GetComponent<dragBird>().BirdStateAtGame == BirdState.Thrown)
        {
            transform.position = new Vector3(Mathf.Clamp(nowbird.transform.position.x, leftCam.transform.position.x, rightCam.transform.position.x),
                transform.position.y, transform.position.z);
        }
        
    }

   public void updateScore()
    {
        s = "SCORE: " + score.ToString();
        m_text.text = s;
        m_text.lineSpacing = 0.5f;
    }

   IEnumerator WaitGame()
   {
       yield return new WaitForSeconds(3f);
   }

   IEnumerator StarEnable(float t)
   {
       yield return new WaitForSeconds(t);
       if (score > 1000)
       {
           Star1.SetActive(true);
           yield return new WaitForSeconds(t);
       }

       if (score > 10000)
       {
           Star2.SetActive(true);
           yield return new WaitForSeconds(t);
       }
       if (score > 15000)
       {
           Star3.SetActive(true);
           yield return new WaitForSeconds(t);
       }
   }


}        
