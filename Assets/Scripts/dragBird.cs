﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine.UI;
public class dragBird : MonoBehaviour
{
    public LineRenderer stringBack;
    public LineRenderer stringFront;
    public Camera cam;
    public GameObject smallSmokeFlying;
    public GameObject bigSmokeFlying;
    public GameObject Feather;
    public GameObject smokeColli;
    public GameObject Scorealive;

    private int wasdrag = 0;
    private AudioSource[] BirdAudios;
    private AudioSource BirdFlyingAudio;
    private AudioSource BirdDieAudio;
    private AudioSource BirdShotedAudio;
    private AudioSource DragStringAudio;

    //variables for game anim and calculate its attack
    private float radius;
    private SpringJoint2D spring;
    private bool clicked;
    private float stretchLimit = 1.0f;
    private Transform slingshot;
    private Ray mouseRay;
    private Ray leftRay;
    private float stretchSquare;
    private Vector3 StartPosition;
    private bool coll = false;
    private float delay;
    private Vector2 velocityX;
    private Vector3 BirdLastPosition;//to find when we draw a smoke
   
    private int mark = 0;
    private GameManage G;


    //mark = 0 if that is big smoke, and 1 is small smoke
    private int _countFlyingAudio = 0;
    private int i;
    private int _firstshotframe;
    [HideInInspector]
    public bool isAnim;
    public Animator BirdAnim;
    public BirdState BirdStateAtGame;
    public GameState StateOfGameinBird;
    public int _countbird;
    public bool _FlightRouteEnable;
    public int numberOfbird;
    public List<GameObject> Birdsmokes;
    void Awake()
    {
        spring = GetComponent<SpringJoint2D>();
        slingshot = spring.connectedBody.transform;
        stringBack.enabled = false;
        stringFront.enabled = false;
    }

    // Use this for initialization
    void Start()
    {
        Scorealive.transform.position = transform.position;
        Scorealive.SetActive(false);
        wasdrag = 0;
        StateOfGameinBird = GameState.Start;
        BirdStateAtGame = BirdState.Off;
        StartPosition = transform.position;
        BirdLastPosition = StartPosition;

        BirdAnim = GetComponent<Animator>();
        isAnim = false;
        BirdAudios = GetComponents<AudioSource>();

        BirdFlyingAudio = BirdAudios[0];
        BirdDieAudio = BirdAudios[1];
        BirdShotedAudio = BirdAudios[2];
        DragStringAudio = BirdAudios[3];
        BirdFlyingAudio.loop = false;
        BirdDieAudio.loop = false;
        DragStringAudio.loop = false;
        Birdsmokes = new List<GameObject>();

        BirdShotedAudio.loop = false;
        _firstshotframe = 1;
        _FlightRouteEnable = false;
        _countbird = 1;
        i = 0;
        delay = 0;
    }


    // Update is called once per frame
    void Update()
    {
        if (BirdStateAtGame == BirdState.Off)
        {
            if (Random.Range(0, 100) < 40)
            {
                BirdBeforeThrowAnimation();
            }
        }
        else
        {
            if (StateOfGameinBird == GameState.BirdMovingToSlingshot && BirdStateAtGame == BirdState.On)
            {
              BirdMoveToSlingshot();
            }

            if (BirdStateAtGame == BirdState.beforeThrown)
            {
                StringSetup();
                stretchSquare = stretchLimit * stretchLimit;
                mouseRay = new Ray(slingshot.position, Vector3.zero);
                leftRay = new Ray(stringFront.transform.position, Vector3.zero);

            }

            if (clicked)
            {
                if (wasdrag == 0)
                {
                    DragStringAudio.Play();
                    wasdrag++;
                }
                Dragging();
            }

            if (spring != null)
            {
                if (delay > 1f)
                {
                    int x = Random.Range(0, 10);
                    delay = 0;

                    if (x < 3)
                    {
                        BirdAnim.Play("OpenMouthAnim");
                    }
                    else if (x < 6)
                    {
                        BirdAnim.Play("isBlink");
                    }
                }
                if (!GetComponent<Rigidbody2D>().isKinematic && velocityX.sqrMagnitude > GetComponent<Rigidbody2D>().velocity.sqrMagnitude)
                {
                    Destroy(spring);
                    GetComponent<Rigidbody2D>().velocity = velocityX;
                }
                if (!clicked)
                {
                    velocityX = GetComponent<Rigidbody2D>().velocity * 30;
                }
                StringUpdate();
            }
            else
            {
                if (DragStringAudio.isPlaying == true)
                {
                    DragStringAudio.Stop();
                }
                if (wasdrag == 1)
                {
                    BirdShotedAudio.Play();
                    wasdrag++;
                }

                stringBack.enabled = false;
                stringFront.enabled = false;

                BirdStateAtGame = BirdState.Thrown;

                if (_firstshotframe == 1 && numberOfbird > 1)
                {
                    DisableFlightRoute();
                    _firstshotframe++;
                }

                BirdFlyingAnimation();

                if (GetComponent<Rigidbody2D>().velocity.magnitude != 0 && coll == true) BirdCollAnimation();


                if (GetComponent<Transform>().position.x < -8 || GetComponent<Transform>().position.x > 17 ||
                   GetComponent<Transform>().position.y >= 5 || GetComponent<Rigidbody2D>().velocity.magnitude == 0)
                {

                    _FlightRouteEnable = true;
                    BirdFlyingAudio.Stop();
                    Instantiate(Feather, transform.position, Quaternion.identity);
                    Instantiate(smokeColli, transform.position, Quaternion.identity);
                    BirdDieAudio.Play();
                    BirdDieAudio.loop = false;//bird died sound isn't loop
                    gameObject.SetActive(false);

                    BirdStateAtGame = BirdState.Die;
                    GetComponent<GameManage>().cameraposition = GetComponent<GameManage>().transform.position;
                    this.gameObject.layer = -1;
                    //this.enabled = false;
                    this.gameObject.SetActive(false);
                    Destroy(this.gameObject);
                }
            }
        }
    }

    void Dragging()//drag slingshot
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 fromslingshot = mousePos - slingshot.position;
        if (fromslingshot.sqrMagnitude > stretchSquare)
        {
            mouseRay.direction = fromslingshot;
            mousePos = mouseRay.GetPoint(stretchLimit);
        }
        mousePos.z = 0f;
        transform.position = mousePos;
        GetComponent<Transform>().position = mousePos;
    }

    void StringUpdate()
    {
        Vector2 projectile = GetComponent<Transform>().position - stringFront.transform.position;
        leftRay.direction = projectile;
        radius = GetComponent<CircleCollider2D>().radius;
        Vector3 hold = leftRay.GetPoint(projectile.magnitude + radius);
        stringBack.SetPosition(1, hold);//with catapult
        stringFront.SetPosition(1, hold);
    }

    public void DisableFlightRoute()
    {
        Birdsmokes = new List<GameObject>(GameObject.FindGameObjectsWithTag("Smoke"));

        foreach (GameObject s in Birdsmokes)
        {
            s.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(s);
        }
    }

    void BirdBeforeThrowAnimation()
    {
        int x;
        delay += Time.deltaTime;
        if (delay > 1f)
        {
            x = Random.Range(0, 10);
            delay = 0;

            if (x < 3)
            {
                BirdAnim.Play("OpenMouthAnim");
            }
            else if (x < 6)
            {
                BirdAnim.Play("isBlink");
            }
            else if(x < 8 && BirdStateAtGame != BirdState.Off && StateOfGameinBird != GameState.BirdMovingToSlingshot)
            {
                GetComponent<Rigidbody2D>().isKinematic = false;
                GetComponent<Rigidbody2D>().AddForce(new Vector2 (transform.position.x, Random.Range(0, 30)));
                GetComponent<Rigidbody2D>().isKinematic = true;
            }
            else
            {
                BirdAnim.Play("IdleAnim");
            }
        }
    }



    public void BirdFlyingAnimation()
    {
        GameObject smoke1;
        GameObject smoke2;

        if (coll == false)
        {
            BirdAnim.Play("BirdFlyingAnim");
            if (BirdFlyingAudio.isPlaying == false && _countFlyingAudio < 1)
            {
                BirdFlyingAudio.loop = false;
                BirdFlyingAudio.Play();
                _countFlyingAudio = 1;
            }
            float distance = Vector2.Distance(BirdLastPosition, transform.position);
            if (distance >= 0.5f)
            {
                mark++;

                if (mark > 50)//bird fly too far 
                {
                    BirdFlyingAudio.Stop();

                    BirdDieAudio.Play();
                    BirdDieAudio.loop = false;//bird died sound isn't loop
                    gameObject.SetActive(false);

                    BirdStateAtGame = BirdState.Die;
                    GetComponent<GameManage>().cameraposition = GetComponent<GameManage>().transform.position;
                    this.enabled = false;
                }
                else
                {
                    if (mark % 2 == 0)
                    {
                        smoke1 = (GameObject)Instantiate(bigSmokeFlying, transform.position, Quaternion.identity);
                        smoke1.tag = "Smoke";
                        BirdLastPosition = transform.position;
                        //Birdsmokes.Add(smoke1);
                    }
                    else
                    {
                        smoke2 = (GameObject)Instantiate(smallSmokeFlying, transform.position, Quaternion.identity);
                        smoke2.tag = "Smoke";
                        BirdLastPosition = transform.position;
                        // Birdsmokes.Add(smoke2);
                    }
                }

            }
        }
    }

    void BirdMoveToSlingshot()
    {
        Vector3 ShotPosition = stringFront.transform.position;
        float timeMoveToSlingshot = Vector2.Distance(transform.position, ShotPosition) / 2f;
        transform.position = Vector3.MoveTowards(transform.position, stringFront.transform.position, timeMoveToSlingshot);
        if (transform.position == stringFront.transform.position)
        {
            StateOfGameinBird = GameState.Playing;
            BirdStateAtGame = BirdState.beforeThrown;
        }
    }

    void BirdCollAnimation()
    {
        BirdAnim.Play("BirdCollAnim");
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (BirdStateAtGame == BirdState.Thrown)
        {
            coll = true;
            if (GetComponent<Rigidbody2D>().velocity.magnitude >= 3f)
            {
                Instantiate(smokeColli, transform.position, Quaternion.identity);
                Destroy(smokeColli, 1f);
                Instantiate(Feather, transform.position, Random.rotationUniform);
                Instantiate(Feather, transform.position, Random.rotationUniform);
                Instantiate(Feather, transform.position, Random.rotationUniform);
            }        

        }
    }

    void StringSetup()
    {
        stringBack.enabled = true;
        stringFront.enabled = true;
        stringBack.SetPosition(0, stringBack.transform.position);
        stringFront.SetPosition(0, stringFront.transform.position);
        stringBack.sortingOrder = 2;
        stringFront.sortingOrder = 4;
        stringFront.sortingLayerName = "Foreground";
        stringBack.sortingLayerName = "Foreground";
    }

    void OnMouseDown()
    {
        BirdStateAtGame = BirdState.beforeThrown;
        spring.enabled = false;
        clicked = true;
    }

    void OnMouseUp()
    {
        spring.enabled = true;
        GetComponent<Rigidbody2D>().isKinematic = false;
        clicked = false;
    }

}