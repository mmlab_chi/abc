﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
    private AudioSource WoodDestroyed;
    public float Health = 100f;
    private GameManage Gamemanage;
    private GameObject Game;
   
    void Start()
    {
        Game = GameObject.FindGameObjectWithTag("MainCamera");
        Gamemanage = Game.GetComponent<GameManage>();
        WoodDestroyed = GetComponent<AudioSource>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;        
        float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 5;
        if (damage > 100)
        {
            WoodDestroyed.Play();
            Gamemanage.score += 100;
            Gamemanage.updateScore();
        }
        Health -= damage;
      
        if (Health <= 0)
        {

            Gamemanage.score += 1000;
            Gamemanage.updateScore();
            Destroy(this.gameObject);
        }
    }
}
