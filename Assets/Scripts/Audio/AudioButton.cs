﻿using UnityEngine;
using System.Collections;

public class AudioButton : MonoBehaviour {
    public AudioSource ButtonBack;
    public AudioSource ButtonNext;
    public void AudioClick(int clicknext)//clicknext = 0: back button, clicknext = 1: next scene
    {
        if (clicknext == 0)
        {
            ButtonBack.loop = false;
            ButtonBack.Play();
        }
        else
        {
            ButtonNext.loop = false;
            ButtonNext.Play();
        }
    }
}
