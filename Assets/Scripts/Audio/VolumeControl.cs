﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class VolumeControl : MonoBehaviour {
    public Camera cam;
    public Button VolBut;
    private AudioListener audio;
    private Animator anim;

    public void PressedVolumeButton()//turn Volume 1 is on; 0 is off
    {
        audio = cam.GetComponent<AudioListener>();
        anim = VolBut.GetComponent<Animator>();
        if(anim != null) Debug.Log("get");
        if (audio.isActiveAndEnabled == true)
        {
            Debug.Log("voloff");
            audio.enabled = false;
            anim.StartPlayback();
            anim.Play("VolOff");
        }
        else
        {
            audio.enabled = true;
            anim.Play("Normal");
        }
    }
}
