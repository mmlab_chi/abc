﻿using UnityEngine;
using System.Collections;

public class Audiodestroy : MonoBehaviour {

    private AudioSource Title;
	// Use this for initialization
    void Start()
    {
        Title = GetComponent<AudioSource>();
        if (Title.isPlaying == false) Title.Play();
        else
        {
            DontDestroyOnLoad(Title);
        }
    }

    public void ContinueAudio(int iscontinueaudio)
    {
        if (iscontinueaudio == 0)
        {
            Destroy(Title);
        }
        else
        {
            DontDestroyOnLoad(Title);
        }
    }
}
