﻿using UnityEngine;
using System.Collections;

public class SelectStage : MonoBehaviour {
    
    void Start()
    {
        //mở tới màn đã chơi
        if (PlayerPrefs.HasKey("CurLevel") == false && PlayerPrefs.HasKey("CurStage") == false)//chưa chơi gì cả
        {
            Debug.Log("open");
            PlayerPrefs.SetInt("CurLevel", 0);
            PlayerPrefs.SetInt("CurStage", 1);
            PlayerPrefs.Save();
            if (PlayerPrefs.HasKey("CurLevel") == true && PlayerPrefs.HasKey("CurStage") == true)
            {
                Debug.Log("openok");
            }
        }
        UnlockLevel();
    }

    public void UnlockLevel()
    {
        Debug.Log("curlevel = " + PlayerPrefs.GetInt("CurLevel").ToString());
        for (int i = 0; i <= LockStage.level; i++)
        {
            if (i < PlayerPrefs.GetInt("CurLevel"))
            {
                GameObject.Find("LockedLevel" + i.ToString()).SetActive(false);
                for (int j = 1; j <= LockStage.stages; j++)
                {
                    UnlockStage(j);
                }
            }
            else if (i == PlayerPrefs.GetInt("CurLevel"))
            {
                for (int j = 1; j <= PlayerPrefs.GetInt("CurStage"); j++)
                {
                    Debug.Log("j = " + j);
                    UnlockStage(j);
                }
            }
        }
    }

    void UnlockStage(int stagenum)
    {

        Debug.Log("Unlock stage" + stagenum.ToString());
        if (stagenum >= 2)
        {
            GameObject.Find("LockedLevel" + stagenum.ToString()).SetActive(false);        
        }
        string s = "HighScore" + stagenum.ToString();

        PlayerPrefs.Save();
    }
}
