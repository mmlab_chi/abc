﻿using UnityEngine;
using System.Collections;

public class BirdFlyingPlayScene : MonoBehaviour {
    public GameObject bird;
    float time = 0;
	// Update is called once per frame
	void Update ()
    {
        time += Time.deltaTime;
        if (time > 3f)
        {
            StartCoroutine(BirdFly());
            time = 0;
        }
	}
    IEnumerator BirdFly()
    {
        Instantiate(bird, new Vector3(Random.Range(-3, 3), -3, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);
        Destroy(bird);
    }
}
