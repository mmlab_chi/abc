﻿using UnityEngine;
using System.Collections;

public class BirdBackGround : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(100, 300), Random.Range(300, 500)));        
	}

    void Update()
    {
        if (transform.position.y < -3)
        {
            enabled = false;
            Destroy(gameObject);
        }
    }
	
}
