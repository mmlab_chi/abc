﻿using UnityEngine;
using System.Collections;

public class Pig : MonoBehaviour {
    public float Health = 200f;
    public GameObject score;
    public GameObject PigDieSmoke;


    private Animator PigAnim;
    private bool coll = false;
    private AudioSource pigAS;
    private GameManage Gamemanage;
    private GameObject Game;
    int timehidescore = 0;
    // Use this for initialization
	void Start () {
        PigAnim = GetComponent<Animator>();
        pigAS = GetComponent<AudioSource>(); 
        pigAS.Play();
        Game = GameObject.FindGameObjectWithTag("MainCamera");
        Gamemanage = Game.GetComponent<GameManage>();

	}

    void OnCollisionEnter2D(Collision2D col)
    {
        coll = true;
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;
        if (col.gameObject.tag == "bird")
        {
            Health = 0;
            StartCoroutine(PigDie());
        }
        else
        {
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 20;
            Health -= damage;
            if(damage > 100) Gamemanage.score += 500;
           
            Gamemanage.updateScore();

            if (Health <= 0)
            {
                StartCoroutine(PigDie());
            }
        }
    }

    IEnumerator PigDie()
    {
        if (timehidescore == 0)
        {
            Instantiate(score, transform.position, Quaternion.identity);
            Instantiate(PigDieSmoke, transform.position, Quaternion.identity);
            timehidescore++;
        }
        Gamemanage.score += 5000;
        Gamemanage.s = "Score: " + Gamemanage.score.ToString();
        Gamemanage.m_text.text = Gamemanage.s;
        Gamemanage._countpig++;
        Gamemanage.score += 5000;
        enabled = false;
        Destroy(gameObject);

        yield return new WaitForSeconds(4f);
    }


	// Update is called once per frame
	void Update () {
        Gamemanage = Game.GetComponent<GameManage>();
        PigAnimation();
       
        if (this.transform.position.x > 17 || Health <=0)
        {
            StartCoroutine(PigDie());
        }
	}

    void PigAnimation()
    {
        if(coll == false)
        {
            if (Random.Range(0, 10) < 2)
            {
                PigAnim.Play("PigBlink");
            }
            else
            {
                PigAnim.Play("PigIdle");
            }
        }
        else
        {
            if (Health >= 150 && Health <= 199) PigAnim.Play("PigHurtLevel1");
            else if (Health >= 80 && Health < 150) PigAnim.Play("PigHurtLevel2");
            else if (Health < 80 && Health > 0) PigAnim.Play("PigHurtLevel3");
        }
    }

}
