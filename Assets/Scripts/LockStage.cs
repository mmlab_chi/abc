﻿using UnityEngine;
using System.Collections;

public class LockStage: MonoBehaviour {
    public static int level = 0;
    public static int stages = 21;
    private int worldIndex;
    private int stageIndex;

    void Start () {
        LockAllGameStages();
    }
    
    public void LockAllGameStages()
    {
        for (int i = 0; i <= level; i++)//lock all level
        {
            
            for (int j = 1; j < stages; j++)//lock all stages
            {
                worldIndex = i;
                stageIndex = j;
                if(!PlayerPrefs.HasKey("stage "+worldIndex.ToString() + stageIndex.ToString()))
                {
                    PlayerPrefs.SetInt("stage "+worldIndex.ToString() + stageIndex.ToString(), 0);//tạo và lock các stage
                    PlayerPrefs.Save();
                }
            }
        }
    }
}
