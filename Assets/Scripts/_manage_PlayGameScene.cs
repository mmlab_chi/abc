﻿using UnityEngine;
using System.Collections;

public class _manage_PlayGameScene : MonoBehaviour {

    public GameObject selectlevel;
    public GameObject selectstage;
    void Awake()
    {
        ActivePanel(1);
    }

    //choose = 1 enable: playScene, choose = 2 enable SelectLevel, choose = 3 enable SelectStage
    public void ActivePanel(int choose)
    {
        switch(choose)
        {
            case 1:
                {
                    selectstage.SetActive(false);
                    selectlevel.SetActive(false);
                    break;
                }
            case 2:
                {
                    selectlevel.SetActive(true);
                    selectstage.SetActive(false);
                    break;
                }
            case 3:
                {
                    selectstage.SetActive(true);
                    selectlevel.SetActive(false);
                    break;
                }
        }
    }
}
